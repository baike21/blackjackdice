# Name:Blackjack Dice Part I
# Function: Play blackjack with computer
# Date: 2017 July 4th
# Environment:Python 3.6.1
import random
PLAYER_MIN_SCORE = 15
DEALER_MIN_SCORE = 17

# reads and validates the user's choice of h or s (hit or stand).
def get_choice():
    choice = input('| Please choose hit or stand (h/s):')
    while choice != 'h' and choice != 's':
        choice = input('| Please choose hit or stand (h/s):')
    return choice

# plays out the player's hand
def deal_players_hand():
    die1 = random.randint(1, 10)
    die2 = random.randint(1, 10)
    # custom roll of one situation
    if die1 == 1:
        die1 = 11
    elif die1 != 1 and die2 == 1:
        die2 = 11
    players_total = die1 + die2
    print('| Player hand:', die1, ' + ', die2, ' = ', players_total)
    print('|')
    if players_total == 21:
        # player wins 21 at the first roll
        return players_total
    else:
        choice = get_choice()
        # player's turn loop
        while choice == 'h' or choice == 's':
            if choice == 'h':
                new_roll = random.randint(1, 10)
                if new_roll == 1 and (players_total + 11 <= 21):
                    new_roll = 11
                print('| Player hand:', players_total, ' + ', new_roll, ' = ', players_total + new_roll)
                players_total += new_roll
                if players_total > 21:
                    print('| PLAYER BUSTS!')
                    return players_total
            elif choice == 's':
                if players_total < PLAYER_MIN_SCORE:
                    print('| Cannot stand on value less than ', PLAYER_MIN_SCORE, '!')
                    new_roll = random.randint(1, 10)
                    if new_roll == 1 and (players_total + 11 <= 21):
                        new_roll = 11
                    print('| Player hand:', players_total, ' + ', new_roll, ' = ', players_total + new_roll)
                    players_total += new_roll
                if players_total > 21:
                    print('| PLAYER BUSTS!')
                    return players_total
                else:
                    return players_total
            choice = get_choice()
            
# plays out the dealer's hand
def deal_dealers_hand(die1):
    # rolls the second 10-sided dice
    die2 = random.randint(1, 10)
    # custom roll of (1) situation
    if die1 != 11 and die2 == 1:
        die2 = 11
    dealers_total = die1 + die2
    print('| Dealer hand:', die1, ' + ', die2, ' = ', dealers_total)

    # dealer's turn loop
    while dealers_total < DEALER_MIN_SCORE:
        new_roll = random.randint(1, 10)
        if new_roll == 1 and (dealers_total + 11 <= 21):
            new_roll = 11
        print('| Dealer hand: ', dealers_total, ' + ', new_roll, ' = ', dealers_total + new_roll)
        dealers_total += new_roll
        
    if dealers_total > 21:
        print('| DEALER BUSTS!')
    return dealers_total

# validate the game winner
def determine_winner(players_hand, dealers_hand):
    # Dealer wins
    if players_hand > 21:
        # Player lose if bust 
        print('|\n| Dealer = ', dealers_hand, ' Player = ', players_hand, '*** Dealer Wins! ***')
        return 0
    else:
        if dealers_hand > 21:
            # Dealer lose if bust, as players_hand is less than 21
            print('|\n| Dealer = ', dealers_hand, ' Player = ', players_hand, '*** Player Wins! ***')
            return 3 
        else:
            if players_hand == dealers_hand:
                # Game draw (push)
                print('|\n| Dealer = ', dealers_hand, ' Player = ', players_hand, '*** Push - no winners. ***')
                return 1
            elif players_hand > dealers_hand:
                # Player wins
                print('|\n| Dealer = ', dealers_hand, ' Player = ', players_hand, '*** Player Wins! ***')
                return 3
            else:
                # Dealer wins 
                print('|\n| Dealer = ', dealers_hand, ' Player = ', players_hand, '*** Dealer Wins! ***')
                return 0
            

def play_blackjack():
    total_score = 0
    print("In function play_blackjack!")

    xx = 'y'
    while xx == 'y':
        print('\n--------------   START GAME --------------')
        die1 = random.randint(1, 10)
        if die1 == 1:
            die1 = 11
        print('| Dealer hand: ', die1)
        player = deal_players_hand()
        dealer = deal_dealers_hand(die1)
        point = determine_winner(player, dealer)
        print('----------------  END GAME  --------------\n')
        total_score += point
        print('Your score: ', total_score, ' -  ', end='')
        xx = input('Play again [y|n]? ')
        while xx != 'y' and xx != 'n':
            print('Please enter y or n.', end='')
            xx = input('Play again [y|n]? ')

    return total_score


# Call function play_blackjack() - plays blackJack against the computer
score = play_blackjack()

# Display score to the screen
print('\n\nYour  score is:', score, '\n\n')
